(function($) {

    /**
    *  Change color plugin - collection method
    */ 
    $.fn.changeColor = function(config){
        var config = config || $.changeColor().config;
        return this.each(function() {
            $(this).css("color", config.color);
        });
    }

    /**
    * Weather plugin - static method
    */ 
    $.changeColor = function(){
        return { config: { color: "rgb(255, 0, 0)" } };
    }

}(jQuery));