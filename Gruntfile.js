module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            build: {
                // Your project's source files directory
                src: 'src/*.js',
                // Your project's destination directory
                dest: 'build/<%= pkg.name %>.min.js'
            }
        },
        jasmine : {
            // Your project's source files
            src : 'src/*.js',
            // General options 
            options: {
                // Your Jasmine spec files
                specs : 'specs/*spec.js',
                // Vendors folder
                vendor: [
                    'vendors/*.js',
                ],
                junit: {
                    path: 'output/testresults'
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Load the plugin that provides the "jasmine" task
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    
    // Default task(s).
    grunt.registerTask('default', ['jasmine', 'uglify']);

};