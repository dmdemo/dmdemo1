describe("Change color plugin ", function() {
    
    it("It should have a set of configurations", function(){
        expect($.changeColor().config).toBeDefined();
    });
    
    it("It should have a default color configuration ", function(){
        expect($.changeColor().config.color).toBeDefined();
    });
    
    it("It should allow configuration override", function(){

        var htmlElement = $("<div> <p> Dummy Element </p> </div>");
        var defaultColor = $.changeColor().config.color;
        var expectedColor = "rgb(255, 255, 255)";

        htmlElement.find("p").changeColor({color: expectedColor});
        var appliedColor = htmlElement.find("p").css("color");
        
        expect(defaultColor).not.toBe(expectedColor);
        expect(expectedColor).toBe(appliedColor);

    });
    
});